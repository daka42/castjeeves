sandbox.gui.useframes.RangeSliderFrame module
=============================================

.. automodule:: sandbox.gui.useframes.RangeSliderFrame
    :members:
    :undoc-members:
    :show-inheritance:
