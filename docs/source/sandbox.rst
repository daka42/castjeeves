sandbox package
===============

Subpackages
-----------

.. toctree::

    sandbox.gui
    sandbox.sqltables
    sandbox.tests
    sandbox.util

Submodules
----------

.. toctree::

   sandbox.sandboxer

Module contents
---------------

.. automodule:: sandbox
    :members:
    :undoc-members:
    :show-inheritance:
