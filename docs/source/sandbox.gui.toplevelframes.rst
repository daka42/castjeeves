sandbox.gui.toplevelframes package
==================================

Submodules
----------

.. toctree::

   sandbox.gui.toplevelframes.additionalconstraints_frame
   sandbox.gui.toplevelframes.freeparam_frame
   sandbox.gui.toplevelframes.mainwindow
   sandbox.gui.toplevelframes.metadata_frame
   sandbox.gui.toplevelframes.topframe

Module contents
---------------

.. automodule:: sandbox.gui.toplevelframes
    :members:
    :undoc-members:
    :show-inheritance:
