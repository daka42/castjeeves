sandbox.sqltables.metadata.Metadata module
==========================================

.. automodule:: sandbox.sqltables.metadata.Metadata
    :members:
    :undoc-members:
    :show-inheritance:
