sandbox.sqltables package
=========================

Subpackages
-----------

.. toctree::

    sandbox.sqltables.metadata
    sandbox.sqltables.source_data

Submodules
----------

.. toctree::

   sandbox.sqltables.ExtractMetadataTables
   sandbox.sqltables.ExtractSourceTables
   sandbox.sqltables.TableLoader
   sandbox.sqltables.sourcetable_explorer

Module contents
---------------

.. automodule:: sandbox.sqltables
    :members:
    :undoc-members:
    :show-inheritance:
