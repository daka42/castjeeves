sandbox.gui.useframes.toggleframe module
========================================

.. automodule:: sandbox.gui.useframes.toggleframe
    :members:
    :undoc-members:
    :show-inheritance:
