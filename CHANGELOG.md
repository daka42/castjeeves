# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

Each version should:
- List its release date.
- Group changes to describe their impact on the project, as follows:
*Added* for new features.
*Changed* for changes in existing functionality.
*Deprecated* for once-stable features removed in upcoming releases.
*Removed* for deprecated features removed in this release.
*Fixed* for any bug fixes.
*Security* to invite users to upgrade in case of vulnerabilities.

### [Unreleased]
#### Added

#### Changed

#### Fixed

## [0.0.2] - 2018-09-14
#### Added
- included yaml file for Gitlab CI testing
- added pytest-cov to test runner to generate coverage report

#### Changed
- switched testing utility to pytest
- changed license to GNUGPLv3.0

#### Removed
- removed usage/dependency of 'tqdm' package


## [0.0.1] - 2018-01-05
#### Added
- git source control to this project